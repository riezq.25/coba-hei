class Notification {
  String? title;
  String? body;
  String? data;

  Notification({this.title, this.body, this.data});

  Notification.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    body = json['body'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['body'] = body;
    data['data'] = data;
    return data;
  }
}
