class Notifikasi {
  String? title;
  String? body;
  String? data;
  String? image;

  Notifikasi({this.title, this.body, this.data, this.image});

  Notifikasi.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    body = json['body'];
    body = json['image'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final resp = <String, dynamic>{};
    resp['title'] = title;
    resp['body'] = body;
    resp['data'] = data;
    resp['image'] = image;
    return resp;
  }
}
