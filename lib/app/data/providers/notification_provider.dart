import 'package:get/get.dart';

import '../models/notification_model.dart';

class NotificationProvider extends GetConnect {
  @override
  void onInit() {
    httpClient.defaultDecoder = (map) {
      if (map is Map<String, dynamic>) return Notification.fromJson(map);
      if (map is List)
        return map.map((item) => Notification.fromJson(item)).toList();
    };
    httpClient.baseUrl = 'YOUR-API-URL';
  }

  Future<Notification?> getNotification(int id) async {
    final response = await get('notification/$id');
    return response.body;
  }

  Future<Response<Notification>> postNotification(
          Notification notification) async =>
      await post('notification', notification);
  Future<Response> deleteNotification(int id) async =>
      await delete('notification/$id');
}
