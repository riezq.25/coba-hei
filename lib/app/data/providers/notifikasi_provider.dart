import 'package:get/get.dart';

import '../models/notifikasi_model.dart';

class NotifikasiProvider extends GetConnect {
  @override
  void onInit() {
    httpClient.defaultDecoder = (map) {
      if (map is Map<String, dynamic>) return Notifikasi.fromJson(map);
      if (map is List)
        return map.map((item) => Notifikasi.fromJson(item)).toList();
    };
    httpClient.baseUrl = 'YOUR-API-URL';
  }

  Future<Notifikasi?> getNotifikasi(int id) async {
    final response = await get('notifikasi/$id');
    return response.body;
  }

  Future<Response<Notifikasi>> postNotifikasi(Notifikasi notifikasi) async =>
      await post('notifikasi', notifikasi);
  Future<Response> deleteNotifikasi(int id) async =>
      await delete('notifikasi/$id');
}
