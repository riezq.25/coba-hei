import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:hei_mobile/app/data/models/notifikasi_model.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HomeView'),
        centerTitle: true,
      ),
      body: Center(
        child: Obx(
          () => ListView.builder(
            itemBuilder: (context, index) {
              Notifikasi notif = controller.notifications[index];
              return Dismissible(
                key: UniqueKey(),
                onDismissed: (direction) {
                  controller.removeNotification(index);
                },
                child: ListTile(
                  leading: (notif.image != null)
                      ? Image(image: NetworkImage(notif.image!))
                      : Text(index.toString()),
                  title: Text(notif.title.toString()),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        notif.body.toString(),
                      ),
                      Text(
                        notif.data.toString(),
                      ),
                    ],
                  ),
                ),
              );
            },
            itemCount: controller.notifications.length,
          ),
        ),
      ),
    );
  }
}
