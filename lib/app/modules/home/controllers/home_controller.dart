import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hei_mobile/app/data/models/notifikasi_model.dart';

class HomeController extends GetxController {
  //TODO: Implement HomeController

  final count = 0.obs;
  final List<Notifikasi> notifications = <Notifikasi>[].obs;

  late FirebaseMessaging messaging;
  String? notificationText;

  void removeNotification(index) {
    if (Get.isSnackbarOpen) {
      Get.back();
    }
    print(index);
    notifications.removeAt(index);
    print(notifications.toString());
    update();
    Get.snackbar(
      "Sukses",
      "Berhasil menghapus notifikasi",
      duration: Duration(
        seconds: 2,
      ),
    );
  }

  @override
  void onInit() {
    late FirebaseMessaging messaging;
    String? notificationText;

    messaging = FirebaseMessaging.instance;
    messaging.subscribeToTopic("messaging");
    messaging.getToken().then((value) {
      print(value);
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      notifications.add(Notifikasi(
          title: event.notification!.title,
          body: event.notification!.body,
          image: event.notification!.android!.imageUrl,
          data: event.data.toString()));

      Get.snackbar(event.notification!.title!, event.notification!.body!);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      // print('Message clicked!');
    });
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
