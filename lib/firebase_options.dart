// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyBbxzkq5MmQaGWFpLxHeR4Gl2vVq61x818',
    appId: '1:797661872227:web:55d911f5a769f0e6bcb856',
    messagingSenderId: '797661872227',
    projectId: 'halal-export-indonesia',
    authDomain: 'halal-export-indonesia.firebaseapp.com',
    storageBucket: 'halal-export-indonesia.appspot.com',
    measurementId: 'G-C07E3SJ64E',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyBYV0SXsoOiaMrHei2C9jG2U5Rx2ZGKjaY',
    appId: '1:797661872227:android:e532b11e98668742bcb856',
    messagingSenderId: '797661872227',
    projectId: 'halal-export-indonesia',
    storageBucket: 'halal-export-indonesia.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyANzICe4RAkaQb65xFS2UlcIiT69JwfWVY',
    appId: '1:797661872227:ios:28ab8467a8772d70bcb856',
    messagingSenderId: '797661872227',
    projectId: 'halal-export-indonesia',
    storageBucket: 'halal-export-indonesia.appspot.com',
    iosClientId: '797661872227-3ovk2f41qat70hp4b8vr0e675fk4uoov.apps.googleusercontent.com',
    iosBundleId: 'com.example.heiMobile',
  );
}
